(function () {
    gadawelApp.controller('loginController', function ($scope) {

            $scope.showRegisterScreen = function () {
                $scope.showScreen = "register";
            };
            $scope.showReset = function () {
                $scope.showScreen = "resetPass";
            };
            $scope.emailRegExp = "/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/";
            $scope.passwordRegExp = "/^[a-z0-9_-]{6,18}$/";

            $scope.userRoles = [
                {
                    id: 1,
                    name: 'مدير مدرسة'
                },
                {
                    id: 2,
                    name: 'طالب'
                },
                {
                    id: 3,
                    name: 'مدرس'
                },
                {
                    id: 4,
                    name: 'ولي أمر'
                }
            ];
            $scope.userRole = 0;
        }
    );
}());
